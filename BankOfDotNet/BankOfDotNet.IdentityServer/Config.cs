﻿using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankOfDotNet.IdentityServer
{
    public class Config
    {
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile()
            };
        }


        public static IEnumerable<ApiResource> GetAllApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource("bankOfDotNetApi", "Customer Api for NankOfDotNetApi")
            };
        }

        public static List<TestUser> GetUsers()
        {
            return new List<TestUser>
            {
                new TestUser
                {
                    SubjectId = "1",
                    Username = "mradis",
                    Password = "password"
                },
                 new TestUser
                {
                    SubjectId = "2",
                    Username = "mradis2",
                    Password = "password"
                } ,
                 new TestUser
                {
                    SubjectId = "3",
                    Username = "mradis3",
                    Password = "password"
                },
            };
        }

        //Resource Owner Password Flow(user involved, trusted 1st party apps(spa , js , native 1st party ))

        //Authorization Code Flow(user involved, google, facebook, etc,  web app (server app), )

        //Implicit Flow (user involved, server side web apps)

        //Hybrid Flow (user involved, Combination of implicit and Authorization code, native apps, server side web apps, native desktop and mobile apps )

        //Client-Credential based grantType (No user involved. A re often used server to server or machine to machine apps trusted 1st party sources)

        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>
            {
                //Client-Credential based grantType
                new Client
                {
                    ClientId="client",
                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedScopes = { "bankOfDotNetApi" }
                },

                //Resource Owner Password grantType
                new Client
                {
                    ClientId = "ro.client",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,

                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedScopes = { "bankOfDotNetApi" }
                },

                //Implicit Flow Grand Type
                new Client
                {
                    //matches the clientId of BankOfDotNet.MvcClient startup class at ConfigureServices at AddOpenIdConnect options
                    ClientId = "mvc",
                    ClientName= "MVC Client",
                    AllowedGrantTypes = GrantTypes.Implicit,


                    //the user is redirected from the client  to identityServer4
                    RedirectUris= { "http://localhost:5003/signin-oidc"},
                    PostLogoutRedirectUris = { "http://localhost:5003/signout-callback-oidc"},

                    //AllowedScopes for this Client
                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile
                    }
                }
            };
        }
    }
}
