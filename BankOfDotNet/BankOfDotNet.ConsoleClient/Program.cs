﻿using IdentityModel.Client;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BankOfDotNet.ConsoleClient
{
    class Program
    {
        public static bool JsonArray { get; private set; }

        public static void Main(string[] args) => MainAsync().GetAwaiter().GetResult();

        private static async Task MainAsync()
        {
            var discoRO = await DiscoveryClient.GetAsync("http://localhost:5000");
            if (discoRO.IsError)
            {
                Console.WriteLine(discoRO.Error);
                return;
            }

            //Grab a bearer token using Client Credential Flow Grant Type
            var tokenClientRO = new TokenClient(discoRO.TokenEndpoint, "ro.client", "secret");
            var tokenResponceRO = await tokenClientRO.RequestResourceOwnerPasswordAsync("mradis","password","bankOfDotNetApi");

            if (tokenResponceRO.IsError)
            {
                Console.WriteLine(tokenResponceRO.Error);
                return;
            }

            Console.WriteLine(tokenResponceRO.Json);
            Console.WriteLine("\n\n");




            //discover all endpoints using metadata of identity server
            var disco = await DiscoveryClient.GetAsync("http://localhost:5000");
            if (disco.IsError)
            {
                Console.WriteLine(disco.Error);
                return;
            }
            //Grab a bearer token
            var tokenClient = new TokenClient(disco.TokenEndpoint, "client", "secret");
            var tokenResponce = await tokenClient.RequestClientCredentialsAsync("bankOfDotNetApi");

            if (tokenResponce.IsError)
            {
                Console.WriteLine(tokenResponce.Error);
                return;
            }

            Console.WriteLine(tokenResponce.Json);
            Console.WriteLine("\n\n");

            //Consume our customer API
            var client = new HttpClient();
            client.SetBearerToken(tokenResponce.AccessToken);


            //Serialize a string object as json
            var customerInfo = new StringContent(
                JsonConvert.SerializeObject(
                    new { Id = 10, FirstName = "Marios", LastName = "Radis" }),
                Encoding.UTF8,"application/json");

            var createCustomerResponce = await client.PostAsync("http://localhost:51698/api/customers",customerInfo);

            if (!createCustomerResponce.IsSuccessStatusCode)
            {
                Console.WriteLine(createCustomerResponce.StatusCode);
            }

            var getCustomerResponce = await client.GetAsync("http://localhost:51698/api/customers");
            if (!getCustomerResponce.IsSuccessStatusCode)
            {
                Console.WriteLine(getCustomerResponce.StatusCode);
            }
            else
            {
                var content = await getCustomerResponce.Content.ReadAsStringAsync();
                Console.WriteLine(JArray.Parse(content));
            }
            Console.Read();
        }
    }
}
